<?php

  
class Genre {

  private $id;

  private $genre;





  /**
   *  Constructeur de genre
   *
   *  fabrique une nouvelle genre vide
   */
  
  public function __construct() {
    // rien à faire
  }


  /**
   *  Magic pour imprimer
   *
   *  Fonction Magic retournant une chaine de caracteres imprimable
   *  pour imprimer facilement un Ouvrage
   *
   *  @return String
   */
 /* public function __toString() {
        return "[". __CLASS__ . "] id : ". $this->id . ":
				   genre  ". $this->genre  .  ;
  }*/

  /**
   *   Getter generrique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   *   @return mixed new attribute value
   */
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }



  /**
   *   Suppression dans la base
   *
   *   Supprime la ligne dans la table corrsepondant à l'objet courant
   *   L'objet doit posséder un OID
   */
  public function delete() {
    
    $pdo = Base::getConnection();

    $sth = $pdo->prepare('DELETE FROM genre WHERE id = :id');
    
    $nb = $sth->execute(array(':id'=>$id));


    return $nb;


    /**
     *  A ECRIRE
     *  CONSTRUIT PUIS EXECUTE LA REQUETE
     *  DELETE FROM Categorie Where id = xxx
     *  RETOURNE LE NOMBRE DE LIGNES SUPPRIMEES
     *
     */
	 
	 
	 
  }
		
		
  /**
   *   Insertion dans la base
   *
   *   Insère l'objet comme une nouvelle ligne dans la table
   *   l'objet doit posséder  un code_rayon
   *
   *   @return int nombre de lignes insérées
   */									
  public function insert() {

   /*
    *  A ECRIRE :
    *  CONSTRUIT LA REQUETE
    *  INSERT INTO Type VALUES (null, 'genre'
    *  PUIS PLACE LA VALEUR DE ID (AUTO-INCREMENT)
    *  DANS L'OBJET COURANT
    *  UTILISE LA METHODE LastInsertId
    *
    *
    */

      $pdo = Base::getConnection();
      
      $sth = $pdo->prepare('INSERT INTO genre VALUES (null, :genre)');      
      $sth->execute(array(':genre'=>$this->genre));

  }
		

 /**
   *   Finder sur ID
   *
   *   Retrouve la ligne de la table correspondant au ID passé en paramètre,
   *   retourne un objet
   *  
   *   @static
   *   @param integer $id OID to find
   *   @return Categorie renvoie un objet de genre Categorie
   */
    public static function findById($id) {
      $query = "select genre from genre where id=". " $id ";
      //echo $query;
      $pdo = Base::getConnection();
      $dbres = $pdo->query($query);
      
      $d=$dbres->fetch(PDO::FETCH_OBJ) ;

      /**
       *   A COMPLETER : CREER UN OBJET A PARTIR DE LA LIGNE
       *   OBJET INSTANCE DE LA CLASSE Categorie
       *
       */
	
	   
    }

    
    /**
     *   Finder All
     *
     *   Renvoie toutes les lignes de la table categorie
     *   sous la forme d'un tableau d'objets
     *  
     *   @static
     *   @return Array renvoie un tableau de categorie
     */
    
    public static function findAll() {

    /**
     *    A ECRIRE ENTIEREMENT
     *    SELECTIONNE TOUTES LES LIGNES DE LA TABLE
     *    ET LES RETOURNE SOUS LA FORME D'UN TABLEAU D'OBJETS
     *
     *
     */
      try{
        $query = "SELECT * FROM genre";
        $pdo = Base::getConnection();
        $dbRes = $pdo->query($query);

        $fAll = $dbRes->fetchAll(PDO::FETCH_OBJ);

        $tabRes = array();

        foreach ($fAll as $c) {
          $o = new Genre();
          $o->id = $c->id;
          $o->genre = $c->genre;

          $tabRes[] = $o;
        }
      } catch (PDOExecption $e){
        throw new PDOException("Error Processing Request" .$e->getMessage());
      }

        return $tabRes;
    }




}



?>
