<?php 

/**
* 
*/
class Base
{
	private static $db_link;

	private static function connect(){
		$config = parse_ini_file("config.ini");

		$host = $config['hostname'];
		$user = $config['user'];
		$pass = $config['password'];
		$dbname = $config['dbname'];

		try {
			$dsn = "mysql:host=$host;dbname=$dbname";

			$db = new PDO($dsn, $user, $pass, array(PDO::ERRMODE_EXCEPTION=>true, PDO::ATTR_PERSISTENT=>true));
			$db->exec("SET CHARACTER SET utf8");

		} catch (PDOException $e) {
			throw new PDOException("connection error: $dsn".$e->getMessage(). '<br/>');	
		}

		return $db;	
	}
	
	


	public static function getConnection(){

		if(isset(self::$db_link)){
			return self::$db_link;
		} else {
			self::$db_link = self::connect();
			return self::$db_link;
		}
	}

}