<?php

  class Loan{
	  
	  private $id;

	  private $departureDate;

	  private $entryDate;

	  private $idAdh;
	  
	  private $idDoc;

   public function __construc(){
	   //rien
   }	  
   
   
   public function __get($attr_name){
	   if(property_exists(__CLASS__,$attr_name)){
		   return $this->$attr_name;
	   }
	   $emess = __CLASS__.":unknown member $att_name (getAttr)";
	   throw new Exception($emess,45);
   }
   
   
   public function __set($att_name,$att_val){
	   if(property_exists(__CLASS__,$attr_name)){
		   $this->$attr_name=$attr_val;
		   return $this->$attr_name;
	   }
	   $emess = __CLASS__.":unknown member $attr_name (setAttr)";
	   throw new Exception($emess,45);
   }
   
    /*INSERT*/
   
   public function insert(){
	    $pdo = Base::getConnection();
 
      $sth = $pdo->prepare('INSERT INTO loan (departureDate, entryDate, idAdh, idDoc) VALUES ( :departureDate, :entryDate, :idAdh, :idDoc)');   
      $sth->execute(array(':departureDate'=>$this->departureDate, ':entryDate'=>$this->entryDate, ':idAdh'=>$this->idAdh, ':id_Doc'=>$this->idDoc));

   }
   
  public static function findById($id) {
      $query = "SELECT * from loan where idDoc=$id and entryDate <> 'Rendu' ";
      $pdo = Base::getConnection();
      $dbres = $pdo->query($query);
      
      $d=$dbres->fetch(PDO::FETCH_OBJ);      

      $o = new Loan();
      $o->id = $d->id;
      $o->departureDate = $d->departureDate;
      $o->entryDate = $d->entryDate;
      $o->idAdh = $d->idAdh;
      $o->idDoc = $d->idDoc;

      return $o;

    }

}

?>