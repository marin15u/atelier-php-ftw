<?php

function __autoload($class_name){
	$tab = array('Controleur', 'Model', 'Vue');

	foreach ($tab as $d) {
		if (file_exists($d."/".$class_name.".php")) {
			require_once($d."/".$class_name.".php");
		}
	}	
}
