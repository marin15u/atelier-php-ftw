<?php


class Vue{


  private $top, $mid, $bot, $document, $genre, $type, $loan;

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }

  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function __construct(){ }

  public function afficheGeneral($select){
    switch ( $select ) {
      case 'ListeDocuments':
        $this->top = self::afficheMenu();
        $this->mid = self::afficheListDocuments();                   
        break;

      case 'DetailDocument':
        $this->top = self::afficheMenu();
        $this->mid = self::afficheDetailsDocuments();                   
        break;        
      
      default:
        # code...
        break;
    }


    $msg = "<!DOCTYPE html>
            <html>
              <head>
                  <title>Medianet &bull; Recherche et consultation de documents en ligne</title>
                  <meta charset='UTF-8' />
                  <link rel='stylesheet' href='stylesheets/screen.css'>
                  <link rel='icon' type='image/png' href='images/livre.ico' /> <!-- Favicon du site -->
              </head>

          <body>
          <div id='content'>
            <div class='row'>
              <header class='offset4 span4'><a href='index.php'><img src='images/header.png' alt='Banniere'></a></header>
            </div>";

    $msg .=   $this->top; /* Menu */
    $msg .=   $this->mid; /* Documents */

   

    $msg .=   "
            <div class='row'>
              <section class='span12'>          
                <footer>

                <p>Copyright GKMCDAMR WebDesign</p>

                <p>
                  <a href='http://jigsaw.w3.org/css-validator/'>
                  <img style='border:0;width:88px;height:31px'
                      src='http://jigsaw.w3.org/css-validator/images/vcss'
                      alt='CSS Valide !' />
                  </a>

                  <a href='http://validator.w3.org/check'>
                    <img src='Images/html5.png' alt='HTML 5 Valide !'>
                  </a>
                  
                </p>

                </footer>
              </section>
            </div>
          </div>";
              
    echo $msg;

  }


  private function afficheListDocuments(){
  
    $res = "";

    foreach ($this->document as $d) {

      $res .= "<a href='index.php?action=details&amp;id=" .$d->id. "'>
                <section class='span4 doc'>
                  <section class='span5'><img src='".$d->image."' alt='images'></section>
                  <section class='span7'>
                  <h3>".$d->title."</h3>

                  <p> de <b>".($d->author)." </b></p>
                  <br>
                  <p class='desc'>".substr(nl2br($d->descriptive), 0, 70). '[...]' . "</p><br>";
                  if($d->status == 'Disponible'){
     $res .=      "<p><span class='available'><b>". $d->status ."</b></span></p>";
                  } else {
     $res .=      "<p><span class='noAvailable'><b>". $d->status ."</b></span></p>";
                  }

                  

      $res .=   "</section> 
                </section>
              </a>" ;
    }
   
    return $res;

  }

  private function afficheMenu(){

    $res = "<div class='row'>
              <section class='offset2 span8'>
                <nav>
                  <form method=POST action='index.php?action=search'>
                    <input type='search' placeholder='Recherche par mot clé' name='search'>
                    <select name='type' id='type'><option value=''> </option> ";
                    foreach ($this->type as $t) {
                        $res .= "<option value=".$t->id.">".$t->type."</option>";
                      }
                      $res .= "</select>
                    <select name='genre' id='genre'> <option value=''> </option>";
                    foreach ($this->genre as $g) {
                      $res .= "<option value=".$g->id.">".$g->genre."</option>";
                      }
                      $res .= "</select> 
                    <input type='submit' value='Go !'>
                  </form>
                </nav>
              </section>
           </div>";

    return $res;
  }

  private function afficheDetailsDocuments(){
  
    $res = "";

      $res .= "<section class='span12'>
                <section class='span2'>
                  <img src='".$this->document->image."' alt='Preview du document'>
                </section>

                <section class='span10'>
                  <h1>".$this->document->title."</h1>

                  <h2> ".$this->document->author."</h2>
                  <p><b>Date de publication : </b>";

                      $originalDate = $this->document->publicationDate;
                      $newDate = date("d-m-Y", strtotime($originalDate));
                      
      $res .=     $newDate."</p>";
                  if($this->document->status == 'Disponible'){
     $res .=      "<p><b>Disponibilité : </b><span class='available'>". $this->document->status ."</span></p>";
                  } else {
     $res .=      "<p><span class='noAvailable'><b>Date de retour : </b>". $this->loan->entryDate ."</span></p>";
                  }

     $res .=      "<p><b>Description :</b> <br>"
                      .nl2br($this->document->descriptive)."</p>
                
                  <form action='index.php'> <input type=submit value='Retour Accueil'> </form>
                </section>";

     $res .= "</section>";
         
    return $res;

  }
}