<?php

Class Controleur{



	public function dispatch($get){

		if(isset($_GET['action'])){

			switch ($_GET['action']) {


				case 'details':
					self::detailDoc();
					break;

				case 'search':
					self::search();
					break;

				
				
				default:
					# code...
					break;
			}
		}

		else{
			self::defaut();			
		}

	}

	private function defaut(){

		$v = new Vue();
		$t = Type::findAll();
		$g = Genre::findAll();
		$d = Document::findAll();

		$v->genre = $g;		
		$v->type = $t;
		$v->document = $d;

		echo $v->afficheGeneral('ListeDocuments');

	}

	private function detailDoc(){

		$v = new Vue();
		$t = Type::findAll();
		$g = Genre::findAll();
		$d = Document::findById($_GET['id']);
		
		$v->genre = $g;		
		$v->type = $t;
		$v->document = $d;

		if($d->status != 'Disponible'){
			$l = Loan::findById($d->id);
			$v->loan = $l;
		}

		echo $v->afficheGeneral('DetailDocument');

	}

	private function search(){

		$v = new Vue();
		$t = Type::findAll();
		$g = Genre::findAll();
		$d = Document::search();

		$v->genre = $g;		
		$v->type = $t;
		$v->document = $d;

		echo $v->afficheGeneral('ListeDocuments');

	}

}