<?php

Class Controleur{



	public function dispatch($get){

		if(isset($_GET['action'])){

			switch ($_GET['action']) {


				case 'formEmprunt':
					self::formEmprunt();
					break;

				case 'formRetour':
					self::formRetour();
					break;

				case 'validEmprunt':
					self::validEmprunt();
					break;

				case 'validRetour':
					self::validRetour();
					break;

				case 'formAddType':
					self::formAddType();
					break;

				case 'validAddType':
					self::validAddType();
					break;

				case 'formAddGenre':
					self::formAddGenre();
					break;

				case 'validAddGenre':
					self::validAddGenre();
					break;

				case 'formAddAdherent':
					self::formAddAdherent();
					break;

				case 'validAddAdherent':
					self::validAddAdherent();
					break;
				
				default:
					# code...
					break;
			}
		}

		else{
			self::defaut();			
		}

	}

	private function defaut(){

		$v = new Vue();

		echo $v->afficheGeneral('Accueil');

	}

	private function formAddAdherent(){
		$v = new Vue();
		echo $v->afficheGeneral('formAddAdherent');
	}

	private function validAddAdherent(){
		$v = new Vue();

		$a = new Adherent();
		$a->name = $_POST['name'];
		$a->lastName = $_POST['lastName'];
		$a->mail = $_POST['email'];
		$a->address = $_POST['adress'];
		$a->dateRegister = date("Y-m-d");
		$a->insert();

		$user = Adherent::findLastAdherent();

		$v->user = $user;

		echo $v->afficheGeneral('validAddAdherent');

	}

	private function formAddGenre(){
		$v = new Vue();
		echo $v->afficheGeneral('formAddGenre');
	}

	private function validAddGenre(){
		$v = new Vue();

		$g = new Genre();
		$g->genre = $_POST['genre'];
		$g->insert();

		echo $v->afficheGeneral('Accueil');
	}

	private function formAddType(){
		$v = new Vue();
		echo $v->afficheGeneral('formAddType');
	}

	private function validAddType(){
		$v = new Vue();

		$t = new Type();
		$t->type = $_POST['type'];
		$t->insert();

		echo $v->afficheGeneral('Accueil');
	}

	private function formEmprunt(){

		$v = new Vue();

		echo $v->afficheGeneral('formEmprunt');

	}

	private function formRetour(){

		$v = new Vue();

		echo $v->afficheGeneral('formRetour');

	}



	private function validEmprunt(){
		
		$v = new Vue();

		$j=0;
		$doc = null;

		extract($_POST);

		$dateActuelle = date('y-m-d');
		$dateRetour = date('y-m-d', strtotime('+15 days'));

		if(count($tab) > 1){									// SI IL Y A PLUS D'UNE REFERENCE on ajoute chaque document dans un tableau

				
				

				while($j < count($tab)){						

					$d = Document::findById($tab[$j]);			// ON RECUPERE LE DOCUMENT POUR FAIRE LA REQUETE
					$d->updateStatusEmprunt();					// ON UPDATE SON STATUS	
					$d = Document::findById($tab[$j]);			// ON RECUPERE LE DOCUMENT MIS A JOUR
					$doc[] = $d;								// AJOUT DANS EN TABLEAU pour faire $v->document = $doc

					$l = new Loan();
					$l->departureDate = $dateActuelle;
					$l->entryDate = $dateRetour;
					$l->idAdh = $idUser;
					$l->idDoc = $tab[$j]; 							// OU $l->idDoc = $d->id;
					$l->insert();								// ON AJOUTE UN EMPRUNT

				$j++;

				}

		}else{													// SINON on récupère simplement le document

				
				$doc = Document::findById($tab[$j]);
				$doc->updateStatusEmprunt();					// ON MET A JOUR LE STATUS
				$doc = Document::findById($tab[$j]);

				$l = new Loan();
				$l->departureDate = $dateActuelle;
				$l->entryDate = $dateRetour;
				$l->idAdh = $idUser;
				$l->idDoc = $tab[$j]; 								// OU $l->idDoc = $d->id;
				$l->insert();									// ON AJOUTE UN EMPRUNT

		}


		
		

		$v->document = $doc;

		$v->afficheGeneral('recapitulatifEmprunt');


		}




	private function validRetour(){
		
		$v = new Vue();		

		$j=0;
		

		extract($_POST);


		if(count($tab) > 1){									// SI IL Y A PLUS D'UNE REFERENCE on ajoute chaque document, emprunts, types et genres dans un tableau				

				while($j < count($tab)){						

					$d = Document::findById($tab[$j]);			// ON RECUPERE LE DOCUMENT
					$d->updateStatusRetour();					// ON UPDATE SON STATUS
					$d = Document::findById($tab[$j]);			// ON RECUPERE LE DOCUMENT MIS A JOUR
					$doc[] = $d;								// AJOUT DANS EN TABLEAU pour faire $v->document = $doc

					$l = Loan::findById($tab[$j]);
					$l->update();								// ON UPDATE LA DATE RETOUR en "Rendu"
					$loan[] = $l;

					$t = Type::findById($d->idType);
					$type[] = $t;
					$g = Genre::findById($d->idGenre);
					$genre[] = $g;

					$j++;

					$idUser= $l->idAdh;

				}

		}else{													// SINON on récupère simplement les objets

				$doc = Document::findById($tab[$j]);			// ON PREND LE DOCUMENT POUR LA REQUETE
				$doc->updateStatusRetour();						// ON MET A JOUR LE STATUS
				$doc = Document::findById($tab[$j]);			// ON REPREND LE DOCUMENT MIS A JOUR

				$l = Loan::findById($tab[$j]);
				$l->update();									// ON UPDATE LA DATE RETOUR en "Rendu"
				$loan = $l;


				$t = Type::findById($doc->idType);					// ON DEFINIT TYPE ET GENRE
				$type = $t;
				$g = Genre::findById($doc->idGenre);
				$genre = $g;

				$idUser= $l->idAdh;


		}	

		$loanNonRendu = Loan::findByIdUser($idUser);			// ET SI IL N'Y A QU'UN SEUL NON RENDU CA NE FONCTIONNE PAS

		if(count($loanNonRendu) > 1){									// SI IL Y A PLUS D'UNE REFERENCE on ajoute chaque document, emprunts, types et genres dans un tableau		
			$j=0;		

				while($j < count($loanNonRendu)){						

					$docu = Document::findById($loanNonRendu[$j]->idDoc);			// ON RECUPERE LE DOCUMENT
					$nonRendu[] = $docu;												

					$t = Type::findById($docu->idType);					// On définit type et genre
					$type[] = $t;
					$g = Genre::findById($docu->idGenre);
					$genre[] = $g;

					$j++;
				}

		}else{													// SINON on récupère simplement les objets

				$doc = Document::findById($loanNonRendu[$j]->idDoc);			// ON PREND LE DOCUMENT POUR LA REQUETE

				echo 'test';
				$t = Type::findById($doc->idType);
				$type = $t;
				$g = Genre::findById($doc->idGenre);
				$genre = $g;
		}		


		
		

		$v->document = $doc;		// Emprunts + documents rendus
		$v->loan = $loan;	

		$v->nonRendu = $nonRendu;		// Emprunts + documents non rendus par l'adhérent
		$v->loan2 = $loanNonRendu;
		
		$v->type = $type;
		$v->genre = $genre;

		$v->afficheGeneral('recapitulatifRetour');


		}
	
	

	
}