<?php


class Vue{


  private $top, $mid, $bot, $document, $genre, $type, $user, $nonRendu, $loan, $loan2;

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }

  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function __construct(){ }


  public function afficheGeneral($select){
    switch ( $select ) {
      case 'Accueil':
        //$this->top = self::affiche;
        $this->mid = self::afficheMenu();                 
        break;

      case 'formEmprunt':
        $this->mid = self::afficheFormEmprunt();                
        break;

      case 'formRetour':
        $this->mid = self::afficheFormRetour();                
        break;

      case 'recapitulatifEmprunt':
        $this->mid = self::afficheRecapEmprunt();                 
        break;

      case 'recapitulatifRetour':
        //$this->top = self::afficheMenu();
        $this->mid = self::afficheRecapRetour();                 
        break;

      case 'formAddType':
        $this->mid = self::afficheAddType();
        break;

      case 'formAddGenre':
        $this->mid = self::afficheAddGenre();
        break;

      case 'formAddAdherent':
        $this->mid = self::afficheAddAdherent();
        break;

      case 'validAddAdherent':
        $this->mid = self::validAddAdherent();
        break;
      
      default:
        # code...
        break;
    }


    $msg = "<!DOCTYPE html>
          <html>
            <head>
                <title>Medianet &bull; Recherche et consultation de documents en ligne</title>
                <meta charset='UTF-8' />
                <link rel='stylesheet' href='stylesheets/screen.css'>
                <link rel='icon' type='image/png' href='Images/livre.ico' /> <!-- Favicon du site -->
            </head>

        <body>
        <div id='content'>
          <div class='row'>
            <header class='offset4 span4'><a href='index.php'><img src='Images/header.png' alt='Banniere'></a></header>
          </div>";

  $msg .=   $this->top; /* Menu */
  $msg .=   $this->mid; /* Documents */

 

  $msg .=   "
          <div class='row'>
            <section class='span12'>          
              <footer>

              <p>Copyright GKMCDAMR WebDesign</p>

              <p>
                <a href='http://jigsaw.w3.org/css-validator/'>
                <img style='border:0;width:88px;height:31px'
                    src='http://jigsaw.w3.org/css-validator/images/vcss'
                    alt='CSS Valide !' />
                </a>

                <a href='http://validator.w3.org/check'>
                  <img src='Images/html5.png' alt='HTML 5 Valide !'>
                </a>
                
              </p>

              </footer>
            </section>
          </div>
        </div>";
    
    echo $msg;

  }


  private function afficheListDocuments(){
  
    $res = "";

    foreach ($this->document as $d) {

      $res .= "<a href='index.php?action=details&id=" .$d->id. "'><section>

                <h1>".$d->title."</h1>

                <p> de ".($d->author)."</p>
                <p>".nl2br($d->descriptive)."</p>" ;

      $res .= "</section></a>" ;
    }
   
    return $res;

  }

  private function afficheMenu(){

    $res = "<div class='row'>
          <section class='offset2 span8'>
            <nav>
              <form  method='POST' action='index.php?action=formEmprunt'>
                <input type='number' name='nbRef' placeholder='Nombre d emprunts'>
                
                <input type='submit' value='Go !'>
              </form>

              <form method='POST' action='index.php?action=formRetour'>
                <input type='number' name='nbRef' placeholder='Nombre de retours'>
                
                <input type='submit' value='Go !'>
              </form>

              <form method='POST' action='index.php?action=formAddType'>
                <input type='submit' value='Ajouter un type de document'>
              </form>

              <form method='POST' action='index.php?action=formAddGenre'>
                <input type='submit' value='Ajouter un genre de document'>
              </form>

              <form method='POST' action='index.php?action=formAddAdherent'>
                <input type='submit' value='Ajouter un nouvel adhérent'>
              </form>
            </nav>
          </section>
        </div>";

    return $res;
  }

  private function afficheFormEmprunt(){

    //$tab[] = "";

    $res = "
        <div class='row'>
          <section class='offset2 span8'>
            <nav>
              <h1>Gestion d'un emprunt</h1>
              <form  method='POST' action='index.php?action=validEmprunt'>
                <input type='text' name='idUser' placeholder='id adherent'> <br>"; //ON AJOUTE L'ID ADHERENT EN POST

                $i = 0; 

                while($i != $_POST['nbRef']){

                $res .=  " <input type='text' name='tab[]' placeholder='Reference document'> <br> ";  //ON AJOUTE CHAQUE REFERENCE DANS UN TABLEAU EN POST
                  $i++;
                }     

    $res  .=   "<input type='submit' name ='submit' value='Valider !'>
              </form>
            </nav>
          </section>
        </div>
        ";


    return $res;
  }

  private function afficheFormRetour(){

    $res = "
        <div class='row'>
          <section class='offset2 span8'>
            <nav>
              <h1>Gestion d'un retour</h1>
              <form  method='POST' action='index.php?action=validRetour'>
                <input type='text' name='idUser' placeholder='id adherent'> <br>";

                $i = 0; 

                while($i != $_POST['nbRef']){

                $res .=  " <input type='text' name='tab[]' placeholder='Reference document'> <br>";
                  $i++;
                }     

    $res  .=   " 
                  <input type='submit' name ='submit' value='Valider !'>
              </form>
            </nav>
          </section>
        </div>
        ";


    return $res;
  }

  private function afficheRecapEmprunt(){
  
    $res = "<div class='row'>
              <section class='offset2 span8'>";

    $res .= "<h1>".count($this->document)." document(s) emprunté le ".date("d-m-y").". <br> Date retour : ".date("d-m-y", strtotime('+15 days'))."</h1>

    <hr>";


      if(is_array($this->document)){

        foreach ($this->document as $d) {
            $res .= "<h3>".$d->title."</h3>
                      <h3> de ".($d->author)."</h3>
                      <hr>";
        }
      }else{

         $res .= "<h3>".$this->document->title."</h3>
                  <h3> de ".$this->document->author."</h3>";

    }

    $res .= "<form action='index.php'> <input type=submit value='Retour Accueil'> </form>
            </section>
          </div>";
         
    return $res;

  }

  private function afficheRecapRetour(){
    
      $res = "";

      $i=0;

      $res.= "<div class='row'><section class='offset2 span8'><h1>Nombre de documents rendus:" .count($this->document). "</h1>";

        if(is_array($this->document)){

                  foreach ($this->document as $d) {

                      $res .= "<h3>".$d->title."</h3>

                                <h3> de ".$d->author."</h3>

                                <center>
                                  <p><b> Type :</b>" .$this->type[$i]->type. " </p>
                                </center>

                                <center>
                                  <p> <b>Genre :</b>" .$this->genre[$i]->genre. " </p>
                                </center>

                                " ;
                      $i++;
              }

        $res .=  "</section></div>";

        }else{

           $res .= "<h3>".$this->document->title."</h3>

                    <h3> de ".$this->document->author."</h3>
                    <center>
                      <p><b> Type : </b>".$this->type->type."</p>
                    </center>
                    <center>
                      <p><b> Genre : </b>".$this->genre->genre."</p>
                    </center>";
        $res .=  "</section></div>";

      }

        $res.= "<section class='offset2 span8'><h1>Nombre de documents non rendus:" .count($this->nonRendu). "</h1></section>";

        if(is_array($this->nonRendu)){

                  foreach ($this->nonRendu as $d)  {

          $res .= "<section class='offset2 span8'><h3>".$d->title."</h3>

                                <h3> de ".$d->author."</h3>

                                <center>
                                  <p><b> Type :</b>" .$this->type[$i]->type. " </p>
                                </center>

                                <center>
                                  <p><b> Genre :</b>" .$this->genre[$i]->genre. " </p>
                                </center>

                                " ;

        $res .=  "</section>";
                      $i++;
              }

        }else{

           $res .= "<h3>".$this->nonRendu->title."</h3>

                    <h3> de ".$this->nonRendu->author."</h3>
                    <center>
                      <p><b> Type :</b>".$this->type->type."</p>
                    </center>
                    <center>
                      <p><b> Genre :</b>".$this->genre->genre."</p>
                    </center>";

        $res .=  "</section></div>";

      }

      $res .= "<form action='index.php'> <input type=submit value='Retour Accueil'> </form>";
           
      return $res;

    }


  private function afficheAddType(){
    $res = "<div class='row'>
              <section class='offset2 span8'>
                <h1>Ajout d'un nouveau type</h1>

                <form action='index.php?action=validAddType' method='POST'>
                  <input type='text' id='type' name='type' placeholder='Libelle du nouveau Type'> <br>
                  
                  <input type='submit' value='Valider'>
                </form>

              </section>
            </div>";

    return $res;

  }

  private function afficheAddGenre(){
    $res = "<div class='row'>
              <section class='offset2 span8'>
                <h1>Ajout d'un nouveau Genre</h1>

                <form action='index.php?action=validAddGenre' method='POST'>
                  <input type='text' id='genre' name='genre' placeholder='Libelle du nouveau Genre'> <br>
                  
                  <input type='submit' value='Valider'>
                </form>

              </section>
            </div>";

    return $res;

  }

  private function afficheAddAdherent(){
    $res = "<div class='row'>
              <section class='offset2 span8'>
                <h1>Ajout d'un nouvel adhérent</h1>

                <form action='index.php?action=validAddAdherent' method='POST'>
                  <input type='text' id='name' name='name' placeholder='Nom'> <br>
                  <input type='text' id='lastName' name='lastName' placeholder='Prénom'> <br>
                  <input type='text' id='adress' name='adress' placeholder='Adresse'> <br>
                  <input type='email' id='email' name='email' placeholder='e-mail'> <br>
                  
                  <input type='submit' value='Valider'>
                </form>

              </section>
            </div>";

    return $res;
  }

  private function validAddAdherent(){
    $res = "<div class='row'>
              <section class='offset2 span8'>
                <h1>Fiche du nouvel adhérent</h1>";

    $res .=     "<section class='offset4 span5'> <p><b>N° Adhérent : </b>".$this->user->id."</p>
                  <p><b>Nom : </b>".$this->user->name."</p>
                  <p><b>Prénom : </b>".$this->user->lastName."</p>
                  <p><b>Adresse : </b>".$this->user->address."</p>
                  <p><b>Email : </b>".$this->user->mail."</p>
                  <p><b>Date inscr. : </b>".$this->user->dateRegister."</p>
                  </section>";

    $res .=  "</section>
            </div>";

    return $res;
  }

}