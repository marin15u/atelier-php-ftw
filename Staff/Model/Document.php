<?php

  
class Document {

  private $id;

  private $title;
  
  private $author;

  private $descriptive;

  private $image;

  private $publicationDate;

  private $status;

  private $idType;

  private $idGenre;

  /**
   *  Constructeur de Document
   *
   *  fabrique un document vide
   */
  
  public function __construct() {
    // rien à faire
  }

  /**
   *   Getter generrique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */

  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   *   @return mixed new attribute value
   */
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }



  public function updateStatusEmprunt() {
    
    if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 
    
    $save_query = "update document set status='Indisponible' where id=".$this->id." ";

    $pdo = Base::getConnection();
    $nb=$pdo->exec($save_query);
    
  return $nb;
    
  }

   public function updateStatusRetour() {
    
    if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    }     
    $save_query = "update document set status='Disponible' where id=".$this->id." ";

    $pdo = Base::getConnection();
    $nb=$pdo->exec($save_query);
    
  return $nb;
    
  }

  public static function findByCat($id) {
    try{

      $pdo = Base::getConnection();

      $sth = $pdo->prepare('SELECT * FROM document WHERE idType = :id');      
      $sth->execute(array(':id'=>$id));

      $fAll = $sth->fetchAll(PDO::FETCH_OBJ);

      $tabRes = array();

      foreach ($fAll as $d) {
        $o = new Document();
        //$o->id = $c->id;
        $o->title = $d->title;
        $o->descriptive = $d->descriptive;
        $o->image = $d->Image;
        $o->publicationDate = $d->publicationDate;
        $o->status = $d->status;
        $o->idType = $d->idType;
        $o->idGenre = $d->idGenre;
        
        $tabRes[] = $o;
      }
    } catch (PDOExecption $e){
      throw new PDOException("Error Processing Request" .$e->getMessage());
    }

      return $tabRes;
  }
  
  public static function findAll() {

 
    try{
      $pdo = Base::getConnection();

      $sth = $pdo->prepare('SELECT * FROM document');      
      $sth->execute(array(':id'=>$id));

      $fAll = $sth->fetchAll(PDO::FETCH_OBJ);

      $tabRes = array();

      foreach ($fAll as $d) {
        $o = new Document();
        $o->id = $d->id;
        $o->title = $d->title;
        $o->descriptive = $d->descriptive;
        $o->image = $d->image;
        $o->author = $d->author;
        $o->publicationDate = $d->publicationDate;
        $o->status = $d->status;
        $o->idType = $d->idType;
        $o->idGenre = $d->idGenre;

        $tabRes[] = $o;
      }
    } catch (PDOExecption $e){
      throw new PDOException("Error Processing Request" .$e->getMessage());
    }

      return $tabRes;
  }

  
  public static function findById($id) {
      $query = "select * from document where id= $id ";
      //echo $query;
      $pdo = Base::getConnection();
      $dbres = $pdo->query($query);
      
      $d=$dbres->fetch(PDO::FETCH_OBJ);

      $o = new Document();
      $o->id = $d->id;
      $o->title = $d->title;
      $o->descriptive = $d->descriptive;
      $o->author = $d->author;
      $o->publicationDate = $d->publicationDate;
      $o->status = $d->status;
      $o->idType = $d->idType;
      $o->idGenre = $d->idGenre;

      return $o;

    }


  

} // FIN Class 


?>
