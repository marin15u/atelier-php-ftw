<?php 

  class Adherent{



  private $id;

  private $name;

  private $lastName;

  private $mail;

  private $address;

  private $dateRegister;




  /**
   *  Constructeur de adherent
   *
   *  fabrique une nouvelle adherent vide
   */
  
  public function __construct() {
    // rien à faire
  }



  /**
   *   Getter generrique
   *
   *   fonction d'acces aux attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut accede
   *   et retourne sa valeur.
   *  
   *   @param String $attr_name attribute name 
   *   @return mixed
   */


  public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }
   

  
  /**
   *   Setter generique
   *
   *   fonction de modification des attributs d'un objet.
   *   Recoit en parametre le nom de l'attribut modifie et la nouvelle valeur
   *  
   *   @param String $attr_name attribute name 
   *   @param mixed $attr_val attribute value
   *   @return mixed new attribute value
   */
    public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }

  public function insert(){

    $pdo = Base::getConnection();
      
    $sth = $pdo->prepare('INSERT INTO adherent (name, lastName, mail, address, dateRegister) VALUES (:name, :lastName, :mail, :address, :dateRegister)');      
    $sth->execute(array(':name'=>$this->name, ':lastName'=>$this->lastName, ':mail'=>$this->mail, ':address'=>$this->address, ':dateRegister'=>$this->dateRegister));
	}

  public static function findLastAdherent(){
    $pdo = Base::getConnection();

    $sth = $pdo->prepare('SELECT * FROM adherent ORDER BY id DESC LIMIT 0, 1');
    $sth->execute(array());

    $a = $sth->fetch(PDO::FETCH_OBJ);

    $o = new Adherent();
    $o->id = $a->id;
    $o->name = $a->name;
    $o->lastName = $a->lastName;
    $o->mail = $a->mail;
    $o->address = $a->address;
    $o->dateRegister = $a->dateRegister;

    return $o;
  }


}

?>