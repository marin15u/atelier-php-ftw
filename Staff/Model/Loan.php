<?php

  class Loan{
	  
	  private $id;

	  private $departureDate;

	  private $entryDate;

	  private $idAdh;
	  
	  private $idDoc;

   public function __construc(){
	   //rien
   }	  
   
   
   public function __get($attr_name) {
    if (property_exists( __CLASS__, $attr_name)) { 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
    throw new Exception($emess, 45);
  }

  public function __set($attr_name, $attr_val) {
    if (property_exists( __CLASS__, $attr_name)) {
      $this->$attr_name=$attr_val; 
      return $this->$attr_name;
    } 
    $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
    throw new Exception($emess, 45);
    
  }
   
    /*INSERT*/
   
   public function insert(){
	    $pdo = Base::getConnection();

      $sth = $pdo->prepare('INSERT INTO loan (departureDate, entryDate, idAdh, idDoc) VALUES ( :departureDate, :entryDate, :idAdh, :idDoc)');   
      $sth->execute(array(':departureDate'=>$this->departureDate, ':entryDate'=>$this->entryDate, ':idAdh'=>$this->idAdh, ':idDoc'=>$this->idDoc));

   }
   
  public function update() {
    
    if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 


    
    $save_query = "update loan set entryDate='Rendu' where id=".$this->id." ";



    $pdo = Base::getConnection();
    $nb=$pdo->exec($save_query);
    
    
  }

  public static function findById($id) {
      $query = "select * from loan where idDoc=$id and entryDate <> 'Rendu' ";
      $pdo = Base::getConnection();
      $dbres = $pdo->query($query);
      
      $d=$dbres->fetch(PDO::FETCH_OBJ);

      

      $o = new Loan();
      $o->id = $d->id;
      $o->departureDate = $d->departureDate;
      $o->entryDate = $d->entryDate;
      $o->idAdh = $d->idAdh;
      $o->idDoc = $d->idDoc;

      return $o;

    }

    public static function findByIdUser($id) {


    try{
        $query = "select * from loan where idAdh=$id and entryDate <> 'Rendu' ";
        $pdo = Base::getConnection();
        $dbRes = $pdo->query($query);

        $fAll = $dbRes->fetchAll(PDO::FETCH_OBJ);

        $tabRes = array();

        foreach ($fAll as $d) {
          
          $o = new Loan();
          $o->id = $d->id;
          $o->departureDate = $d->departureDate;
          $o->entryDate = $d->entryDate;
          $o->idAdh = $d->idAdh;
          $o->idDoc = $d->idDoc;

          $tabRes[] = $o;
        }
      } catch (PDOExecption $e){
        throw new PDOException("Error Processing Request" .$e->getMessage());
      }

        return $tabRes;
    }


   
   /*DELETE*/
   
     public function delete() {
    
    $db=Base::getConnection();   

     if (!isset($this->id)) {
      throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
    } 

   $save_query = "delete from loan where id='".$this->id."'" ;
    try{     
        $exec=$db->prepare($save_query);
        $exec->execute();
        
        return $exec->rowCount();
    }
    catch(BaseException $e){ 
      throw new PDOException("Error Delete".$e->getMessage(). '<br/>');
    }
    
    /*SAVE*/
    
    }

}

?>